const axios = require('axios');
// Stage credentials
// const serverUrl = "https://backend-stage.logicdialog.ai"
// const token = ""
// const users = ["659d6e4b3a27eab145336862","659d74183a27eab145336867","659d53d03a27eab14533685d","63f4eba28b0db70009b6ebca","659e83b53a27eab145336870"];
// const client = "himanshu"

// Local credentials

const token = ""
const users = ["659d262a3cedff9ebf000a69","65a0162db3c7a6826ad36bc7","65a0169cb3c7a6826ad36bc8","65a01730b3c7a6826ad36bca"];
const client = "wbb-council"
const serverUrl = "http://localhost:3002";


const isDateBeforeSixDays = (dateString) => {
    const eventDate = new Date(dateString);
    const sixDaysAgo = new Date();
    sixDaysAgo.setDate(sixDaysAgo.getDate() - 6);
  
    return eventDate < sixDaysAgo;
  };
  
  
  const isEventProcessingRequired = (
    agentLang,
    event,
  ) => {
    const message = event?.message;
    // Handle bot msg
    if (
      message &&
      message.type === 'bot-msg' &&
      message?.selectedLang !== agentLang
    ) {
      return true;
    }
  
    // Handle user text
    if (
      message &&
      message?.type === 'text' &&
      !message.formFieldCallback &&
      message?.text?.language !== agentLang
    ) {
      return true;
    }
  
    // Handle user attachment and only update if it is before 6 days
    if (
      message &&
      message?.type === 'attachments' &&
      isDateBeforeSixDays(event.created)
    ) {
      return true;
    }
  
    return false;
  };

  
const fetchAllEvents = async(userId) => {
    let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: `${serverUrl}/api/agent/events/messages?id=${userId}&page=0&pageSize=20&skipProcessEvents=true`,
        headers: { 
          'origin': serverUrl, 
          'referer': serverUrl, 
          'wbb-client': client, 
          'Authorization': `Bearer ${token}`
        }
      };
      
      const response = await axios.request(config)
      return response.data.data;
}


const processEvent = async (eventId, userId) => {
    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: `${serverUrl}/api/agent/events/messages/${eventId}?userId=${userId}&media=false`,
        headers: { 
          'origin': serverUrl, 
          'referer': serverUrl, 
          'wbb-client': client, 
          'Authorization': `Bearer ${token}`
        }
      };
      
      const response = await axios.request(config)
      return response.data.data;
}


const startProcessing = async(userId, agentLanguage = "en")=> {
    console.log("Processing start for user", userId)
    const events = await fetchAllEvents(userId);
    events.forEach((event) => {
    if (
      (event?.message &&
        event?.message.type === 'choice' &&
        isEventProcessingRequired(
            agentLanguage,
          data[data.indexOf(event) + 1],
        )) ||
      isEventProcessingRequired(agentLanguage, event)
    ) {
        processEvent(event._id, userId)
        .then(() => {
            console.log('Processing done for event',event._id )
        })
        .catch(() => {
            console.log('Error in processing event',event._id )
        });
    }
  });
}


async function testApi(users) {
    try {
      const promises = users.map(userId => startProcessing(userId));
      await Promise.all(promises);
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const num = 6;
  for(let n = 1; n <= num; n++){
  testApi(users)
      .then(() => console.log(`All users processed ${n}`))
      .finally(() => console.log(`Final execution ${n}`));
    }