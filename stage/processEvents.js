module.exports = {
    processEventsResponse: (requestParams, response, context, ee, next) => {
      let events;
      try {
        events = JSON.parse(response.body);
        events = events.data || [];
      } catch (error) {
        console.error("Error parsing JSON:", error);
        events = [];
      }
  
      if (events.length === 0) {
        return next();
      }
  
      context.vars.events = events;
      return next();
    }
  };
  